const { response } = require('express');
const { validationResult } = require('express-validator');

const validarCampos = (req, res = response, next) => {
  const errors = validationResult(req);
  // manejos de errores
  if (!errors.isEmpty()) {
    const errorsArray = [];

    const errorsMapped = errors.mapped();

    for (const property in errorsMapped) {
      if (property === 'email' || property === 'password') {
        errorsArray.push(errorsMapped[property].msg);
      }
    }

    const errorsString = errorsArray.join(', ');

    return res.status(400).json({
      ok: false,
      msg: errorsString,
    });
  }

  next();
};

module.exports = { validarCampos };
