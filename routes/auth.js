/*
   Rutas de Usuario / Auth
   host + /api/auth
*/

const { Router } = require('express');
const { check } = require('express-validator');
const router = Router();

const {
  createUser,
  loginUser,
  revalidationToken,
} = require('../controllers/auth');
const { validarJWT } = require('../middlewares/validar-jwt');

const { validarCampos } = require('../middlewares/validar-campos');

router.post(
  '/new',
  [
    check('name', 'El name is requier').not().isEmpty(),
    check('email', 'El email is requier').isEmail(),
    // .custom(async (value) => {
    //   const emailCheck = await getUserByEmail(value);
    //   if (!!emailCheck) return Promise.reject();
    // })
    // .withMessage('Email ya se encuentra registrado'),
    check('password', 'El password debe de ser de 6 caracteres').isLength({
      min: 6,
    }),
    validarCampos,
  ],
  createUser
);

router.post(
  '/',
  [
    check('email', 'El email is requier').isEmail(),
    check('password', 'El password debe de ser de 6 caracteres').isLength({
      min: 6,
    }),
    validarCampos,
  ],
  loginUser
);

router.get('/renew', [validarJWT], revalidationToken);

module.exports = router;
