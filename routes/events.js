/*
   Rutas de los eventos / events
   host + /api/events
*/

const { Router } = require('express');
const { check } = require('express-validator');

const { isDate } = require('../helpers/isDate');
const {
  getEventos,
  crearEventos,
  actualizarEvento,
  eliminarEvento,
} = require('../controllers/events');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

const router = Router();

// Todas pasan por la validacion jwt
router.use(validarJWT);

router.get('/', getEventos);

router.post(
  '/',
  [
    check('title', 'El titulo es obligatorio').not().isEmpty(),
    check('start', 'La fecha de inicio es obligatoria').custom(isDate),
    check('end', 'La fecha de fin es obligatoria').custom(isDate),
    validarCampos,
  ],
  crearEventos
);

router.put(
  '/:id',
  [
    check('title', 'El titulo es obligatorio').not().isEmpty(),
    check('start', 'La fecha de inicio es obligatoria').custom(isDate),
    check('end', 'La fecha de fin es obligatoria').custom(isDate),
    // check('end', 'La fecha fin es obligatoria').isDate(),
    validarCampos,
  ],
  actualizarEvento
);

router.delete('/:id', eliminarEvento);

module.exports = router;
